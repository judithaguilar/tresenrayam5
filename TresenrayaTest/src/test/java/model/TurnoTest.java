package model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TurnoTest {
    private Turno turno;

    @Nested
    class cambiarTurno {

        @Test
        void cambiarTurnoDeXaO() {
            turno = new Turno(Ficha.X);
            turno.cambiarTurno();
            assertEquals(Ficha.O, turno.getFicha());
        }

        @Test
        void cambiarTurnoDeOaX() {
            turno = new Turno(Ficha.O);
            turno.cambiarTurno();
            assertEquals(Ficha.X, turno.getFicha());
        }
    }
}