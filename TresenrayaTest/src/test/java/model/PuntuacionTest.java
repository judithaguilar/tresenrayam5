package model;
import static org.junit.jupiter.api.Assertions.*;

class PuntuacionTest {
    private Puntuacion puntuacion;
    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        puntuacion = new Puntuacion();
    }

    @org.junit.jupiter.api.Test
    void ganaX() {
        puntuacion.ganaX();
        assertEquals(1, puntuacion.getGanaX());
    }

    @org.junit.jupiter.api.Test
    void ganaO() {
        puntuacion.ganaO();
        assertEquals(1, puntuacion.getGanaO());
    }

    @org.junit.jupiter.api.Test
    void reset() {
        puntuacion.ganaO();
        assertEquals(1, puntuacion.getGanaO());

        puntuacion.ganaX();
        assertEquals(1, puntuacion.getGanaX());

        puntuacion.reset();

        assertEquals(0, puntuacion.getGanaO());
        assertEquals(0, puntuacion.getGanaX());
    }
}