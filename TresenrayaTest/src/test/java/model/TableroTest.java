package model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TableroTest {
    private Tablero tablero;

    @BeforeEach
    void setUp() {
        tablero = new Tablero();
    }

    @Test
    void ponerFicha() {
        tablero.ponerFicha(1, Ficha.X);
        assertEquals("X", tablero.getPosicion(1));

        tablero.ponerFicha(2, Ficha.O);
        assertEquals("O", tablero.getPosicion(2));
    }

    @Test
    void posicionLibre() {
        assertTrue(tablero.posicionLibre(1));

        tablero.ponerFicha(1, Ficha.X);
        assertFalse(tablero.posicionLibre(1));
    }

    @Nested
    class estaLleno {

        @Test
        void noEstaLleno() {
            assertFalse(tablero.estaLleno());

            tablero.ponerFicha(1, Ficha.X);
            assertFalse(tablero.estaLleno());

            tablero.ponerFicha(2, Ficha.X);
            assertFalse(tablero.estaLleno());

            tablero.ponerFicha(3, Ficha.X);
            assertFalse(tablero.estaLleno());

            tablero.ponerFicha(4, Ficha.X);
            assertFalse(tablero.estaLleno());

            tablero.ponerFicha(5, Ficha.X);
            assertFalse(tablero.estaLleno());

            tablero.ponerFicha(6, Ficha.X);
            assertFalse(tablero.estaLleno());

            tablero.ponerFicha(7, Ficha.X);
            assertFalse(tablero.estaLleno());

            tablero.ponerFicha(8, Ficha.X);
            assertFalse(tablero.estaLleno());
        }

        @Test
        void siEstaLleno() {
            tablero.ponerFicha(1, Ficha.X);
            tablero.ponerFicha(2, Ficha.X);
            tablero.ponerFicha(3, Ficha.X);
            tablero.ponerFicha(4, Ficha.X);
            tablero.ponerFicha(5, Ficha.X);
            tablero.ponerFicha(6, Ficha.X);
            tablero.ponerFicha(7, Ficha.X);
            tablero.ponerFicha(8, Ficha.X);
            tablero.ponerFicha(9, Ficha.X);
            assertTrue(tablero.estaLleno());
        }
    }

    @Nested
    class hayGanador {

        @Nested
        class hayGanadorX {

            @Test
            void hayGanadorVertical1() {
                tablero.ponerFicha(1, Ficha.X);
                tablero.ponerFicha(4, Ficha.X);
                tablero.ponerFicha(7, Ficha.X);
                assertTrue(tablero.hayGanador());
            }

            @Test
            void hayGanadorVertical2() {
                tablero.ponerFicha(2, Ficha.X);
                tablero.ponerFicha(5, Ficha.X);
                tablero.ponerFicha(8, Ficha.X);
                assertTrue(tablero.hayGanador());
            }

            @Test
            void hayGanadorVertical3() {
                tablero.ponerFicha(3, Ficha.X);
                tablero.ponerFicha(6, Ficha.X);
                tablero.ponerFicha(9, Ficha.X);
                assertTrue(tablero.hayGanador());
            }

            @Test
            void hayGanadorHorizontal1() {
                tablero.ponerFicha(1, Ficha.X);
                tablero.ponerFicha(2, Ficha.X);
                tablero.ponerFicha(3, Ficha.X);
                assertTrue(tablero.hayGanador());
            }

            @Test
            void hayGanadorHorizontal2() {
                tablero.ponerFicha(4, Ficha.X);
                tablero.ponerFicha(5, Ficha.X);
                tablero.ponerFicha(6, Ficha.X);
                assertTrue(tablero.hayGanador());
            }

            @Test
            void hayGanadorHorizontal3() {
                tablero.ponerFicha(7, Ficha.X);
                tablero.ponerFicha(8, Ficha.X);
                tablero.ponerFicha(9, Ficha.X);
                assertTrue(tablero.hayGanador());
            }

            @Test
            void hayGanadorDiagonal1() {
                tablero.ponerFicha(1, Ficha.X);
                tablero.ponerFicha(4, Ficha.X);
                tablero.ponerFicha(7, Ficha.X);
                assertTrue(tablero.hayGanador());
            }

            @Test
            void hayGanadorDiagonal2() {
                tablero.ponerFicha(3, Ficha.X);
                tablero.ponerFicha(5, Ficha.X);
                tablero.ponerFicha(7, Ficha.X);
                assertTrue(tablero.hayGanador());
            }
        }

        @Nested
        class hayGanadorO {

            @Test
            void hayGanadorVertical1() {
                tablero.ponerFicha(1, Ficha.O);
                tablero.ponerFicha(4, Ficha.O);
                tablero.ponerFicha(7, Ficha.O);
                assertTrue(tablero.hayGanador());
            }

            @Test
            void hayGanadorVertical2() {
                tablero.ponerFicha(2, Ficha.O);
                tablero.ponerFicha(5, Ficha.O);
                tablero.ponerFicha(8, Ficha.O);
                assertTrue(tablero.hayGanador());
            }

            @Test
            void hayGanadorVertical3() {
                tablero.ponerFicha(3, Ficha.O);
                tablero.ponerFicha(6, Ficha.O);
                tablero.ponerFicha(9, Ficha.O);
                assertTrue(tablero.hayGanador());
            }

            @Test
            void hayGanadorHorizontal1() {
                tablero.ponerFicha(1, Ficha.O);
                tablero.ponerFicha(2, Ficha.O);
                tablero.ponerFicha(3, Ficha.O);
                assertTrue(tablero.hayGanador());
            }

            @Test
            void hayGanadorHorizontal2() {
                tablero.ponerFicha(4, Ficha.O);
                tablero.ponerFicha(5, Ficha.O);
                tablero.ponerFicha(6, Ficha.O);
                assertTrue(tablero.hayGanador());
            }

            @Test
            void hayGanadorHorizontal3() {
                tablero.ponerFicha(7, Ficha.O);
                tablero.ponerFicha(8, Ficha.O);
                tablero.ponerFicha(9, Ficha.O);
                assertTrue(tablero.hayGanador());
            }

            @Test
            void hayGanadorDiagonal1() {
                tablero.ponerFicha(1, Ficha.O);
                tablero.ponerFicha(4, Ficha.O);
                tablero.ponerFicha(7, Ficha.O);
                assertTrue(tablero.hayGanador());
            }

            @Test
            void hayGanadorDiagonal2() {
                tablero.ponerFicha(3, Ficha.O);
                tablero.ponerFicha(5, Ficha.O);
                tablero.ponerFicha(7, Ficha.O);
                assertTrue(tablero.hayGanador());
            }
        }
    }
}