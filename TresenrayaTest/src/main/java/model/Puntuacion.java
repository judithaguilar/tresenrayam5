package model;

public class Puntuacion {
    private int ganaX;
    private int ganaO;

    public Puntuacion() {
        this.ganaX = 0;
        this.ganaO = 0;
    }

    public void ganaX(){
        this.ganaX++;
    }

    public void ganaO()
    {   //Jud: bug arreglado
        this.ganaO++;
    }

    public int getGanaX() {
        return ganaX;
    }

    public int getGanaO() {
        return ganaO;
    }

    public void reset() {
        this.ganaX = 0;
        this.ganaO = 0;
    }
}
